angular.module("readplus")
.controller("dashboardCtrl", function($scope,$http, $accessToken) {

    $scope.hourly_data = [];
    $scope.daily_data = [];
    $scope.filter = {};
    $scope.daily_tab = "sum";
    $scope.monthly_tab = "sum";

    $scope.get = function(url, params) {

        config = {
            params: params,
            headers: {
                Authorization: "Bearer " + $accessToken
            }
        }

        return $http.get("https://readplus.pergite.com" + url, config);
    }

    $scope.load = function(){

        var params = {params:{filter:$scope.filter}};

         $scope.get("/api/v1/reporting/latest", params).then(function (response) {
                $scope.latest = response.data;
         });
         $scope.get("/api/v1/reporting/spent?p=day", params).then(function (response) {
                $scope.parsePeriodData(response.data, 'daily_data');
         });
         $scope.get("/api/v1/reporting/spent?p=hour", params).then(function (response) {
                $scope.parsePeriodData(response.data, 'hourly_data');
         });

         params.params.func = $scope.monthly_tab;
         $scope.get("/api/v1/reporting/articles?p=month", params).then(function (response) {
               $scope.articles_month = response.data;
         });
         params.params.func = $scope.daily_tab;
          $scope.get("/api/v1/reporting/articles?p=day", params).then(function (response) {
               $scope.articles_day = response.data;
         });
         //$timeout($scope.load,5000);
    };

    $scope.sum = function(collection){

        if (collection[0] == undefined) return 0;

      return collection[0].reduce(function(a, b){ return {y:a.y+b.y, c:a.c+b.c}});
    };

    $scope.parsePeriodData = function(data, collection){

         $scope[collection] = [data.map(function(r){return {t:r.index,y:r.spent/1000/60,c:r.credits*0.0028}
              })];
    };

    $scope.options = {};
    $scope.options.hourly =  {
        responsive: true,

        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                type: 'time',
                time:{unit: 'hour'},

                ticks: {
                    maxRotation: 0,
                    //maxTicksLimit:7,

                }
            }]

        }
    };

    $scope.options.daily =  {
        responsive: true,

        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                type: 'time',
                time:{unit: 'day'},

                ticks: {
                    maxRotation: 0,
                    //maxTicksLimit:7,

                }
            }]

        }
    };

    $scope.load();
})
;
