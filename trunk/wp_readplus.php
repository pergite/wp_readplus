<?php
/**
 * @package wp_readplus
 * @version 1.0
 */

/*
Plugin Name: Glimta plugin
Plugin URI: https://glimta.com/
Description: Provides Glimta micropayments to WordPress blogs
Author: Pergite Software AN
Version: 1.0
Author URI: https://glimta.com/
*/

include_once plugin_dir_path( __FILE__ ) . 'handlers/oauth.php';

add_action('init', 'readplus_authentication_handler');
add_action('wp_head', "readplus_tracking_script");
add_filter('the_content', 'readplus_content');

add_action( 'admin_init', function () {
	register_setting('readplus', 'readplus_client_id');
    register_setting('readplus', 'readplus_client_secret');
    register_setting('readplus', 'readplus_access_token');
});

add_action( 'admin_menu', function () {

	require_once( plugin_dir_path( __FILE__ ) . 'settings.php');
	require_once( plugin_dir_path( __FILE__ ) . 'dashboard.php');

	$parent_slug = 'glimta-dashboard';

	$dashboard = add_menu_page('Glimta Dashboard', 'Glimta', 'manage_options', $parent_slug, 'readplus_dashboard', 'dashicons-awards' );
	add_submenu_page($parent_slug, 'Glimta Dashboard', 'Dashboard', 'manage_options', $parent_slug, 'readplus_dashboard');
	$settings = add_submenu_page($parent_slug, 'Glimta Settings', 'Settings', 'manage_options', 'glimta-settings', 'readplus_settings');

	add_action( 'load-' . $dashboard, 'readplus_load_dashboard' );
});

function readplus_load_dashboard() {
	add_action( 'admin_enqueue_scripts', function() {

        wp_enqueue_script('readplus_angularjs', plugins_url('resources/angular.min.js', __FILE__));
        wp_enqueue_script('readplus_angularuijs', plugins_url('resources/ui-bootstrap-tpls.js', __FILE__));
        wp_enqueue_script('readplus_momenttjs',  plugins_url('resources/moment.js', __FILE__));
        wp_enqueue_script('readplus_angular-moment', plugins_url('resources/angular-moment.min.js', __FILE__));
        wp_enqueue_script('readplus_chartjs',  plugins_url('resources/chart.min.js', __FILE__));
        wp_enqueue_script('readplus_angular-chart', plugins_url('resources/angular-chart.min.js', __FILE__));

        wp_enqueue_style('readplus_bootstrap', plugins_url('resources/bootstrap.min.css', __FILE__));
        wp_enqueue_style('readplus_fontawesome', plugins_url('resources/font-awesome.min.css', __FILE__));

        wp_enqueue_style('readplus_css', plugins_url('wp_readplus.css', __FILE__));
        wp_enqueue_script('readplus_globaljs', plugins_url('global.js', __FILE__));
        wp_enqueue_script('readplus_dashboardjs', plugins_url('dashboard.js', __FILE__));
    } );
}

add_action( 'widgets_init', function () {
	require_once( plugin_dir_path( __FILE__ ) . 'login_widget.php');
    register_widget( 'LoginWidget' );
});

function readplus_tracking_script() {

    if(isset($_REQUEST["RP_User"])) {

        $client_id = get_option('client_id');
        //$author = the_author_meta('display_name', get_post(get_queried_object_id())->post_author );
        global $post;
        $author_id = $post->post_author;
        $author = get_the_author_meta('user_login', $author_id);

        global $wp;
        $current_url = home_url( $wp->request ) . "/";

        $payload = array(
            'author' => $author,
            'url' => $current_url
        );

        $oa = new ReadPlusOAuth();
        $access = $oa->registerAccess($_REQUEST["RP_User"]->Token, $payload);

        $_REQUEST["RP_Access"] = $access["access"];

        if($access["access"]) 
            echo $access["trackingScript"];
    }
}

function readplus_content($content){
    if (isset($_REQUEST["RP_Access"])) {

        if(!$_REQUEST["RP_Access"]) {
            return "<a href='" . site_url() . "/wp_readplus/authenticate?next=" . urlencode(get_post_permalink()) . "'>Sign in to read the full article</a>";
        }

        return $content;
    };

    $pieces = explode("<!--subscriberonly-->", $content);
    if (count($pieces) == 1) {
     return $content;
    }

    $out =  $pieces[0];

    return $out .= "<a href='" . site_url() . "/wp_readplus/authenticate?next=" . urlencode(get_post_permalink()) . "'>Sign in to read the full article</a>";
}

?>
