<?php


class LoginWidget extends WP_Widget {

	function __construct() {
        parent::__construct( false, 'ReadPlus Login Widget' );
	}

    function widget( $args, $instance )  {

        include_once plugin_dir_path( __FILE__ ) . 'login_scripts.php';

        if(!isset($_REQUEST["RP_User"])) {

            $auth_url = site_url() . "/wp_readplus/authenticate";
            $login_code = "document.location.href='" . $auth_url . "?next=' + document.location.href;";
?>
            <div class="readplus-box">
                <div class="readplus-textbox">
                    Sign in using ReadPlus!
                </div>
                <div>
                    <a class="readplus-button login" onclick="readplus_Login(event)" href="#">Sign in</a>
                </div>
            </div>
<?php
        }
        else {
?>
            <div class="readplus-box">
                <div class="readplus-textbox">
                    Thank You for using ReadPlus!
                </div>
                <div>
                    <a class="readplus-button readplus-logout" onclick="readplus_Logout(event)" href="#">Sign out</a>        
                </div>
            </div>
<?php
        }
	}

	function update( $new_instance, $old_instance ) {
	}

	function form( $instance ) {
    }
}

?>
