<?php

include_once plugin_dir_path( __FILE__ ) . '../utils.php';

class ReadPlusUser {

    public $Id = "";
    public $Token = "";
    function __construct($UserId, $AccessToken)  {

        $this->Id = $UserId;
        $this->Token = $AccessToken;
    }
}

class ReadPlusOAuth {

    public static $readplus_url = "https://glimta.com";
    //public static $readplus_url = "https://dev-local.readplus.com";

    function getAccessToken($clientid, $clientsecret, $code, $callback){
            
        $ch = curl_init(self::$readplus_url . "/oauth/token");
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $ch, CURLOPT_ENCODING, "" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
        curl_setopt( $ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic '.base64_encode($clientid . ":" . $clientsecret),
            'Accept: application/json'
        ));
        
        curl_setopt( $ch, CURLOPT_POSTFIELDS, 'redirect_uri=' . urlencode($callback) . '&grant_type=authorization_code&client_id=' . $clientid.'&client_secret=' . $clientsecret . '&code=' . $code);
        $content = curl_exec($ch);
        
        if(curl_error($ch)){
            echo "<b>Response : </b><br>";print_r($content);echo "<br><br>";
            exit( curl_error($ch) );
        }

        $json = json_decode($content, true);
        
        if(!is_array($json) || !isset($json["access_token"])) {
            echo "<b>Response : </b><br>";print_r($content);echo "<br><br>";
            exit('Invalid response received from OAuth Provider. Contact your administrator for more details.');
        }
        
        $access_token = $json["access_token"];
        return $access_token;
    }

    function getMe($access_token) {

        $req = curl_init(self::$readplus_url . "/api/v1/me");
        curl_setopt( $req, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $req, CURLOPT_ENCODING, "" );
        curl_setopt( $req, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $req, CURLOPT_AUTOREFERER, true );
        curl_setopt( $req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $req, CURLOPT_MAXREDIRS, 10 );
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, 0); 
        curl_setopt($req, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $access_token,
            'Accept: application/json'
        ));

        $content = curl_exec($req);

        return json_decode($content, true);
    }

    function registerAccess($access_token, $payload) {

        $req = curl_init(self::$readplus_url . "/api/v1/me/access");
        curl_setopt( $req, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $req, CURLOPT_ENCODING, "" );
        curl_setopt( $req, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $req, CURLOPT_AUTOREFERER, true );
        curl_setopt( $req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $req, CURLOPT_MAXREDIRS, 10 );
        curl_setopt( $req, CURLOPT_POST, true);        
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, 0); 
        curl_setopt($req, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $access_token,
            'Content-Type: application/json',
            'Accept: application/json'
        ));

        curl_setopt($req, CURLOPT_POSTFIELDS, json_encode($payload));
        
        $content = curl_exec($req);

        return json_decode($content, true);
    }

    function registerSite($access_token) {

        $req = curl_init(self::$readplus_url . "/api/v1/sites");
        curl_setopt( $req, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $req, CURLOPT_ENCODING, "" );
        curl_setopt( $req, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $req, CURLOPT_AUTOREFERER, true );
        curl_setopt( $req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $req, CURLOPT_MAXREDIRS, 10 );
        curl_setopt( $req, CURLOPT_POST, true);        
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, 0); 
        curl_setopt($req, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $access_token,
            'Content-Type: application/json',
            'Accept: application/json'
        ));

        $payload = array(
            'name' => get_bloginfo('name'),
            'uri' => site_url(),
            'scope' => 'profile',
            'redirect_uris' => array(site_url() . '/wp_readplus/authenticated'),
            'subscription_level' => 'basic'
        );

        curl_setopt($req, CURLOPT_POSTFIELDS, json_encode($payload));
        
        $content = curl_exec($req);

        return json_decode($content, true);
    }
}

function readplus_authentication_handler() {
    
    $request_uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

    if(readplus_endswith($request_uri, "/wp_readplus/authenticate")) {

        session_start();

        if( isset($_GET["next"]))
            $next = $_GET["next"];
        else 
            $next = site_url();

        $_SESSION["readplus_next"] = urldecode($next);

        $client_id = get_option('readplus_client_id');
        $callback = site_url() . "/wp_readplus/authenticated";

        $authorizationUrl = ReadPlusOAuth::$readplus_url . "/oauth/authorize?client_id=" . $client_id . "&scope=profile&redirect_uri=" . $callback . "&response_type=code&state=";
        header('Location: ' . $authorizationUrl);

        exit;
    }
    else if(readplus_endswith($request_uri, "/wp_readplus/authenticated")) { 

        session_start();

        $client_id = get_option('readplus_client_id');
        $client_secret = get_option('readplus_client_secret');
        $code = $_GET['code'];
        
        $callback = site_url() . "/wp_readplus/authenticated";

        $oa = new ReadPlusOAuth();
        $access_token = $oa->getAccessToken($client_id, $client_secret, $code, $callback);


        /*

        $current_user = wp_get_current_user();
        update_user_meta($current_user->ID, 'readplus_AccessToken', $access_token);

        error_log("current_user");
        error_log($current_user->ID);
        */
        
        $me = $oa->getMe($access_token);
        setcookie("RP_UserId", $me["id"], strtotime('+12 months'), "/");
        setcookie("RP_Token", $access_token, strtotime('+12 months'), "/");

        $_SESSION["readplus_token"] = $access_token;

        $redirect = $_SESSION["readplus_next"];

        if( isset($_GET["final_uri"]))
            $redirect = urldecode($_GET["final_uri"]);

        if($redirect == admin_url('admin.php?page=glimta-dashboard')) {

            update_option("readplus_access_token", $access_token);
        }

        header('Location: ' . $redirect);
        exit;
    }

    if(isset($_COOKIE["RP_UserId"])) {

        $_REQUEST["RP_User"] = new ReadPlusUser($_COOKIE["RP_UserId"], $_COOKIE["RP_Token"]);
    }
}
?>
