<?php

    function readplus_endswith($haystack, $needle) {
        return substr($haystack, -strlen($needle))===$needle;
    }

    function readplus_startswith($haystack, $needle) {
        return substr($haystack, strlen($needle))===$needle;
    }

?>
