<?php

function readplus_dashboard() {

//$current_user = wp_get_current_user();
//$access_token = get_user_meta($current_user->ID, 'readplus_AccessToken', true);

$access_token = get_option("readplus_access_token");

?>
  <script>
    angular.module("readplus")
      .constant("$accessToken", '<?php echo $access_token ?>')
    ;
  </script>
  <h1>Glimta Dashboard</h1>

<?php
    $next = admin_url('admin.php?page=glimta-dashboard');
    $url = site_url() . "/wp_readplus/authenticate?next=" . urlencode($next);

?>
  <a href="<?php echo $url ?>" class="btn btn-primary">Reconnect Your Glimta Publisher Account</a>

<?php
  
  if(!empty($access_token))
  {
?>
    <div ng-app="readplus" ng-controller="dashboardCtrl">

      <div class="row">
          <div class="col-sm-6">

              <div class="card">
                  <div class="card-header"><h5>Time Spent (LAST 24h)</h5><h4>{{ sum(hourly_data).y | number:1  }}min, {{ sum(hourly_data).c | number:2 }}SEK</h4></div>
                  <div class="card-body">

                      <canvas id="bar1" class="chart chart-line"
                        chart-data="hourly_data" chart-options="options.hourly" chart-labels="hours" chart-series="series">
                      </canvas>

                  </div>
              </div>


          </div>
          <div class="col-sm-6">

              <div class="card">
                  <div class="card-header"><h5>Time Spent (Month)</h5><h4>{{ sum(daily_data).y | number:1  }}min, {{ sum(daily_data).c | number:2 }}SEK</h4></div>
                  <div class="card-body">

                      <canvas id="bar2" class="chart chart-line"
                        chart-data="daily_data" chart-options="options.daily">
                      </canvas>

                  </div>
              </div>

          </div>

      </div>

    <div class="row">


<div class="col-sm-6">


    <div class="card">
        <div class="card-header"><h5>Most Time Spent (Day)</h5></div>
        <div class="card-body no-padding">

             <ul class="nav nav-tabs small">
              <li class="nav-item">
                <a ng-click="change_daily_tab('sum')" class="nav-link" ng-class="{'active':daily_tab=='sum'}" href="#">Total</a>
              </li>
              <li class="nav-item">
                <a ng-click="change_daily_tab('mean')" class="nav-link" ng-class="{'active':daily_tab=='mean'}" href="#">Average</a>
              </li>
            </ul>

             <table class="table small">

                 <thead>
                 <tr>
                     <th>Page</th>
                     <th class="text-right">Time Spent</th>
                     <th class="text-right">Earned</th>

                 </tr>
                 </thead>
                 <tr ng-repeat="article in articles_day |limitTo:10">

                    <td><div class="badge badge-light p-1">
                              <div style="text-align: left"><small>{{ article.host }}</small></div>
                              <div>{{ article.title }}</div>
                                <div style="text-align: left"><small><i title="{{ article.path }}">{{ shortenPath(article.path) }}</i></small></div>
                             </div>

                        </td>
                     <td  align="right">{{ article.spent/1000/60 | number:1 }} min</td>
                     <td  align="right">{{ article.credits*0.0028 | number:2 }} SEK</td>

                </tr>

            </table>

        </div>
    </div>



</div>

 <div class="col-sm-6">


    <div class="card">
        <div class="card-header"><h5>Most Time Spent (Month)</h5></div>
        <div class="card-body no-padding">


              <ul class="nav nav-tabs small">
              <li class="nav-item">
                <a ng-click="change_monthly_tab('sum')" class="nav-link" ng-class="{'active':monthly_tab=='sum'}" href="#">Total</a>
              </li>
              <li class="nav-item">
                <a ng-click="change_monthly_tab('mean')" class="nav-link" ng-class="{'active':monthly_tab=='mean'}" href="#">Average</a>
              </li>
            </ul>

             <table class="table small">

                 <thead>
                 <tr>
                     <th>Page</th>
                     <th class="text-right">Time Spent</th>
                     <th class="text-right">Earned</th>

                 </tr>
                 </thead>
                 <tr ng-repeat="article in articles_month |limitTo:10">

                     <td><div class="badge badge-light p-1">
                              <div style="text-align: left"><small>{{ article.host }}</small></div>
                              <div>{{ article.title }}</div>
                                <div style="text-align: left"><small><i title="{{ article.path }}">{{ shortenPath(article.path) }}</i></small></div>
                             </div>

                        </td>
                     <td align="right">{{ article.spent/1000/60 | number:1 }} min</td>
                     <td  align="right">{{ article.credits*0.0028 | number:2 }} SEK</td>

                </tr>

            </table>

        </div>
    </div>



</div>

</div>




    </div>



<?php
  }

  }
?>
