<?php
    $auth_url = site_url() . "/wp_readplus/authenticate";
?>
<style>
    .readplus-button {
        background-color: #339933; 
        color: white; 
        border-width: 1px; 
        border-style:solid; 
        border-color: black; 
        border-radius: 10px; 
        padding: 7px 7px 7px 7px;
    }

    .readplus-logout {
        font-size: smaller;
    }

    .readplus-textbox {
        padding-bottom: 10px;
    }

    .readplus-box {

        position: relative;
        border-width: 1px; 
        border-style:solid; 
        border-color: #339933; 
        border-radius: 10px; 
        padding: 7px 7px 7px 7px;

    }
</style>
<script>

    function readplus_Login(e) {
        e.preventDefault();
        document.location.href = '<?php echo $auth_url ?>?next=' + document.location.href;
    }

    function readplus_Logout(e) {

        e.preventDefault();

        readplus_deleteCookie("RP_UserId", "/");
        readplus_deleteCookie("RP_Token", "/");

	    location.reload();
        //document.location.href = document.location.href;
    }

    function readplus_deleteCookie( name, path, domain ) {
        document.cookie = name + "=" +
        ((path) ? ";path="+path:"")+
        ((domain)?";domain="+domain:"") +
        ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
    }

</script>
