<?php

include_once plugin_dir_path( __FILE__ ) . 'handlers/oauth.php';

add_action( 'admin_action_readplus_auto', 'readplus_auto_admin_action' );
function readplus_auto_admin_action()
{
    if(!session_id())
        session_start();

    $access_token = $_SESSION["readplus_token"];

    if(!isset($access_token)) {

        $redirect = site_url() . "/wp_readplus/authenticate?next=" . admin_url('admin.php?action=readplus_auto');
        wp_redirect($redirect);
        exit;
    }

    $oa = new ReadPlusOAuth();
    $site = $oa->registerSite($access_token);
    
    update_option("readplus_client_id", $site["client_id"]);
    update_option("readplus_client_secret", $site["client_secret"]);

    wp_redirect( admin_url('admin.php?page=readplus_settings' ));
    exit();
}

function readplus_settings() {



?>
	<div class="wrap">
	<h1>Glimta Settings</h1>

    <div class="readplus-alert readplus-alert-info">
        <p>
        Enter your Site's ClientId and ClientSecret from <a href="https://glimta.com/publisher" target="_blank">https://glimta.com/publisher</a> (found under the Developer tab)
        </p>

        <p>
        If You havent already, You need to enter Your WordPress site's "Allowed OAuth callback URL", which is:<br>
        <pre><?php echo site_url() ?>/wp_readplus/authenticated</pre>
        </p>
    </div>

	<form method="post" action="options.php">

<?php
				settings_fields( 'readplus' );
            do_settings_sections( 'readplus' );
            
            $client_id = get_option('readplus_client_id');
            $client_secret = get_option('readplus_client_secret');

?>
			<label>Client Id
				<input type="text" name="readplus_client_id" style="width: 100%;" value="<?php echo esc_attr( $client_id ); ?>" placeholder="ClientId here" />
			</label>
			<label>Client Secret
				<input type="text" name="readplus_client_secret" style="width: 100%;" value="<?php echo esc_attr( $client_secret ); ?>" placeholder="ClientSecret here" />
			</label>

	    <?php submit_button(); ?>

	</form>
	</div>
<?php
}
?>
